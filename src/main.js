import Vue from 'vue'
import Pcom from './pcom.vue'
import store from './store/store.js'

var vm = new Vue({
  el: '#J_com_chat',
  store,
  components: { 
  	'p-com': Pcom
  }
})