import { MUTATION_A, MUTATION_B, MUTATION_C_F, MUTATION_C_A, MUTATION_C_B  } from './mutation-types'

export default {
	[MUTATION_A](state,payload){
		state.dataA = payload;
	}, 
	[MUTATION_B](state,payload){
		state.dataB = payload;
	}, 
	[MUTATION_C_F](state,payload){
		state.dataC.ctoF = payload;
	},
	[MUTATION_C_A](state,payload){
		state.dataC.ctoA = payload;
	},
	[MUTATION_C_B](state,payload){
		state.dataC.ctoB = payload;
	} 
}