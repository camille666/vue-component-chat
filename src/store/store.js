import Vue from 'vue'
import Vuex from 'vuex'
import * as getters from './getters.js'
import mutations from './mutations.js'
import actions from './actions.js'

Vue.use(Vuex)

const state = {
	dataF:{
		ftoA:'一朵红花',
		ftoB:'一朵蓝花',
		ftoC:'一朵绿花'
	},
	dataA:'',
	dataB:'',
	dataC:{
		ctoA:'',
		ctoF:'',
		ctoB:''
	}
}

const store = new Vuex.Store({
	state,
	getters,
	mutations,
	actions
})
export default store