import { MUTATION_A, MUTATION_B, MUTATION_C_A, MUTATION_C_B } from './mutation-types'

export default {
	actionA({commit,state}, payload){
		commit(MUTATION_A, payload);
	},
	actionB({commit,state}, payload){
		commit(MUTATION_B, payload);
	},
	actionCA({commit,state}, payload){
		commit(MUTATION_C_A, payload);
	},
	actionCB({commit,state}, payload){
		commit(MUTATION_C_B, payload);
	}
}